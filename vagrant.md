# vagrant

## Présentation

Vagrant est un outil en ligne de commande codé en ruby permettant de gérer et de configurer automatiquement un ensemble de machines vituelles pour solution technique.

A chaque fois que l'outil est lancé (commande vagrant), il lit le fichier de définition de la solution technique présent __dans le dossier courrant__ : le `Vagrantfile` et réalise les actions demandé en argument à la commande.

L'outil va créer et installer les VM, configurer leur IP et leur hostnames et enfin les "provisionner" (effectuer des actions de configuration).

Une fois la tache exécuté l'outil s'arrete et laisse la solution technique dans l'état souhaité.

![](./images/vagrant.png)

### Vocabulaire :

__Vagrant :__ C’est l'outil et la commande permettant de gérer et de configurer automatiquement un ensemble de machines vituelles

__Une box :__ C’est un template de systeme d’exploitation maintenue et disponible sur l’atlas de vagrant sur internet. Vagrant télécharge les box à la vollé en cas de besoin. (attention à la bande passante internet !)

__Une VM ou le « guest » :__ c’est une machine virtuelle 

__Le provider :__ C’est la solution de virtualisation utilisée : le founisseur de VM. Ici nous utiliserons VirtualBox en local, cependant d’autre provider peuvent être utilisés : KVM, proxmox, vmware (sous licence payante de 78$)

__Les provisions :__ Ce sont les actions de provisionning définies dans le vagrantfile.

__Les provisionneurs :__ c'est les solutions de provisions, exemple le provisioneur `file` permet de placer des fichiers sur la VM, le provisonneur `shell` permet d'exécuter des commandes, etc le provisonneur `ansible` permet de jouer des playbook ansible etc... (on en reparlera plus tard)

## Installation

Vagrant fonctionne avec VirtualBox (pas seulement mais pour le moment c'est la meilleure solution). Vous devez donc avoir VirtualBox installé sur votre laptop.

Pour l'installation de vagrant, Read The Doc : https://www.vagrantup.com/docs/installation/

__Attention sous windows__ les comptes avec caractères accentués ou spécifiques posent problème. Utilisez donc un compte sans accent. Vous eviterez aussi plusieurs problèmes en utilisant la version de VirtualBox 5.2.

> Je ne peux que vous encourager à mettre en place un dualboot et à travailler un peu sous linux ou à utiliser (cmder)[#Installation-de-cmder]

### Installation de cmder

* Téléchargez la __Download Full__ sur cette page (cmder)[https://cmder.net]
* Une fois le téléchargement terminé, dézippez le fichier, et placez le dossier obtenu dans le répertoire que vous voulez
* Exécutez le fichier __Cmder.exe__ en __administrateur__
* Et voilà ! Vous avez un terminal bash, avec plein de commandes bash pré-enregistrées (ls, par exemple, mais aussi git, et d'autres). Vous pouvez donc l'utiliser avec les commandes vagrant aussi, en travaillant dans le dossier avec vos VagrantFile

## Utilisation

Vous travaillez depuis __un dossier projet__ qui contiens un vagrant file et le reste du matériel nécessaire à cette solution technique.

les commandes principales :

* `vagrant up [nom-vm]` : Lance les VM qui sont définies dans le Vagrantfile ou la VM nommée. ces VM seront créées si elle ne sont pas déja existante et dans ce cas les `provisions` seront exécutées
* `vagrant halt [nom-vm]` : Arrête les VM définies dans le Vagrantfile
* `vagrant status [nom-vm]` : vous retourne le status des vm gérées
* `vagrant destroy [nom-vm]` : supprime les VM
* `vagrant provision [--provision-with provision]` : lance les actions de provisionning ou uniquement celle nommée.
* `vagrant ssh [nom-vm]` : vous connecte en ssh à la VM avec __le compte vagrant__

Pour les autres action utilisez l'option `-h` pour avoir de l'aide :

```bash
$ vagrant -h
Usage: vagrant [options] <command> [<args>]

    -v, --version                    Print the version and exit.
    -h, --help                       Print this help.

Common commands:
     box             manages boxes: installation, removal, etc.
     destroy         stops and deletes all traces of the vagrant machine
     global-status   outputs status Vagrant environments for this user
     halt            stops the vagrant machine
     help            shows the help for a subcommand
     init            initializes a new Vagrant environment by creating a Vagrantfile
     login           log in to HashiCorps Vagrant Cloud
     package         packages a running vagrant environment into a box
     plugin          manages plugins: install, uninstall, update, etc.
     port            displays information about guest port mappings
     powershell      connects to machine via powershell remoting
     provision       provisions the vagrant machine
     push            deploys code in this environment to a configured destination
     rdp             connects to machine via RDP
     reload          restarts vagrant machine, loads new Vagrantfile configuration
     resume          resume a suspended vagrant machine
     snapshot        manages snapshots: saving, restoring, etc.
     ssh             connects to machine via SSH
     ssh-config      outputs OpenSSH valid configuration to connect to the machine
     status          outputs status of the vagrant machine
     suspend         suspends the machine
     up              starts and provisions the vagrant environment
     validate        validates the Vagrantfile
     version         prints current and latest Vagrant version

For help on any individual command run `vagrant COMMAND -h`

Additional subcommands are available, but are either more advanced
or not commonly used. To see all subcommands, run the command
`vagrant list-commands`.
```

## Fonctionnement

### les box

Ces template de vm sont disponible sur internet, la commande vagrant les téléchargent en cas de besoin afin que ces template soient disponible sur votre host.  
Si vous ne le préciser pas dans votre Vagrantfile, les box seront mise à jour automatiquement si une nouvelle version existe lors de la création de la VM.

### le dossier .vagrant

Dans __le dossier projet__, vagrant créer un dossier `.vagrant` lui permettant de conserver toutes les informations relatives aux VMs créées. Ce dossier sera ensuite utilisé comme référentiel des machines pour le projet. (sauf exception on positionnera `.vagrant` sera positionnée dans le .gitignore )

### l'accès ssh

Les box contiennent par avance le compte vagrant avec une clef publique authorisée (fichier authorized file) pour laquelle __la clef privée est connue de tous, donc non sécurisée__.
A la création de la VM, un forward de port est mis en place sur l'interface nat de la VM afin de pouvoir se connecter à la VM en ssh. La clef connue est utilisée pour une première connexion elle est immédiatement modifiée par une nouvelle qui est cette fois-ci sécurisée : Une nouvelle paire de clef est créer __par VM__ et stockée dans le dossier .vagrant/machines/$NOMMACHINE/virtualbox/ (dans le cas du provider VirtualBox)

On se connectera à la VM avec la commande :

```bash
$ vagrant ssh $nomvm
```

### le dossier /vagrant sur les VM

A la création et au démarrage d'une VM le contenu de votre dossier projet, le dossier courant, celui-là même contenant votre Vagrantfile, est synchronisé avec rsync sur les VMs sur le dossier /vagrant.
Ansi ce contenu est disponible sur les VM et utilisable pour vos actions de provisionning.

## Le vagrant file

Le vagrant file est du code en ruby. En revanche il n'est pas nécessaire de comprendre le ruby pour créer et modifier les Vagrant files.

Lorsque vous créer un vagrant file, vous definissez les objets `vm`, `provision` et configurez les objets `box` ou `provider` afin de concevoir votre ensemble de VM.

La commande `vagrant init` permet de créer automatiquement un fichier vagrantfile.

Exemple :

```bash
$ vagrant init centos/7
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
$
```

Le fichier `Vagrantfile` ainsi créé contiens pas mal de détails en commentaire sur ce qu'il est possible de faire dans tout les cas vous avez de la doc en ligne : https://www.vagrantup.com/docs/vagrantfile/

le mieux pour comprendre est de faire ce [TD](./TD-vagrant.md)
